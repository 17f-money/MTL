---
title: Some Things to Remember about Metal
date: 2016-03-08
tags: ["example", "code"]
---


- Metal's open alpha began October 30, 2017.
- In order to receive PoPP (the biggest incentive for Metal), one must identify his or herself with Passport ID number or Social Security Number.
- Metal is run by a company, Metallicus, Inc.
- Metallicus is ostensibly partnered with a major bank, and can be used with FDIC-backed bank accounts.  However, the company has not disclosed which bank they are partnered with at this time.