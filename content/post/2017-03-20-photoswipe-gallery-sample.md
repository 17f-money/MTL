---
title: The Statistics
subtitle: Network and Currency Info
date: 2017-03-20
tags: ["example", "photoswipe"]
---


-Total Supply: 66,588,888 MTL

-Available for token sale: 19,588,888

-Reserved for PoPP: 26,341,112

-Reserved for Metallicus employees: 3,360,000

-Remainder (locked for one year): 20,658,888