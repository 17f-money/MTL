﻿---
title: Proof-of-Processed-Payment
subtitle: The Incentive
date: 2017-03-05
tags: ["example", "math"]
---

Metal does not involve transaction fees.  In fact, it actually involves the opposite.
"Proof-of-Processed-Payments" (PoPP) is the system through which Metal introduces new tokens into circulation.
Any transaction involving fiat currency will give 5% of the transaction value to each party (in MTL).


</br></br>


The logic behind this is that ordinary users who wish to simply use cash will eventually build up a reserve of this cryptocurrency.  They will then be compelled to get further involved with cryptocurrency, as they will have accumulated a noteworthy amount of value in the form of MTL.


</br></br>


Note that while one can trade in Metal without being personally identified, one must have proven their identity with either passport ID or social security number in order to receive PoPP.
This is part of a system that prevents PoPP from being abused.  Otherwise, individuals could make transactions with each other repeatedly and obtain endless amounts of MTL.  Metal uses algorithms to prevent this sort of trading, as well as preventing similar plans that involve more than two parties.  Note that the software preventing this is not open-source, and thus one must trust Metallicus Inc.'s statements that they successfully prevent such abuse.