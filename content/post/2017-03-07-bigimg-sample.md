---
title: A Usable Cryptocurrency
subtitle: Using Multiple Images
date: 2017-03-07
tags: ["example", "bigimg"]
bigimg: [{src: "/img/triangle.jpg", desc: "Triangle"}, {src: "/img/sphere.jpg", desc: "Sphere"}, {src: "/img/hexagon.jpg", desc: "Hexagon"}]
---

Unlike other cryptocurrencies, Metal uses a real, FDIC-backed bank account.
This can inherently reassure individuals who would be concerned by holding their money in the form of an un-backed cryptocurrency.
Unfortunately, Metallicus still has not publicly disclosed which bank is backing their currency.

Transactions are claimed to be processesd within one minute, however there is no technical information to back this claim up.